Feature: Adding new transaction template
  In order to quickly add new transactions
  As a user
  I want to be able to create predefined template of an transaction

  Scenario Outline: Adding Template
    Given I often make following transaction
      | name           | amount |
      | <expenseName>  |<expenseAmount>        |
    When I save this template
    Then I should be able to get this template

    Examples: Expenses
      | expenseName          | expenseAmount|
      | Bread                | 3.5    |
      | Pepsi                | 2.20   |
      | A Bag of chips       | 5.32   |