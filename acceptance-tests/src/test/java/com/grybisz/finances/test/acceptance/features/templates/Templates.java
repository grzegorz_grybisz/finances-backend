package com.grybisz.finances.test.acceptance.features.templates;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by Grzesiek on 2017-02-26.
 */
@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/features/templates")
public class Templates {
}

