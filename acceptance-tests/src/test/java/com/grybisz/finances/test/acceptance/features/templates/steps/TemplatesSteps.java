package com.grybisz.finances.test.acceptance.features.templates.steps;

import com.grybisz.finances.templates.TemplatesApplication;
import com.grybisz.finances.templates.dto.CreateTemplateDto;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

/**
 * Created by Grzesiek on 2017-02-26.
 */
@SpringBootTest(classes = TemplatesApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration
public class TemplatesSteps {

    private CreateTemplateDto currentTemplate;

    //TODO change to selfURI? if test will return url without need to cut it
    private String templateId;

    @Value("${local.server.port}")
    int port;

    @Before
    public void configurePorts() {
        RestAssured.port = port;
    }

    @Given("I often make following expense")
    public void prepareExpenses(List<CreateTemplateDto> templates) {
        currentTemplate = templates.get(0);
    }

    @When("I save this template")
    public void addNewTemplate() throws Throwable {
        templateId = given().contentType("application/json")
                .body(currentTemplate)
                .post("/template")
                .then().statusCode(200)
                .and().extract().jsonPath().getString("id");
    }

    @Then("I should be able to get this template")
    public void getTemplate() throws Throwable {

        //Cast to float, because restAssured only compares to float
        float newExpenseAmount = (float) (currentTemplate.getAmount());

        given().contentType("application/json")
                .get("/template/{id}", templateId)
                .then().statusCode(200)
                .and().body("name", equalTo(currentTemplate.getName()))
                .and().body("amount", equalTo(newExpenseAmount));
    }
}