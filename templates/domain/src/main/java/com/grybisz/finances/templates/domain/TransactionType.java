package com.grybisz.finances.templates.domain;

public enum TransactionType {
    EXPENSE, INCOME;
}
