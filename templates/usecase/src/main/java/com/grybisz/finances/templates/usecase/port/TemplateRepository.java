package com.grybisz.finances.templates.usecase.port;

import com.grybisz.finances.templates.domain.Template;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by Grzesiek on 2017-03-04.
 */
public interface TemplateRepository {

    String saveTemplate(Template template);

    Optional<Template> findTemplate(String templateId);

    Collection<Template> getAllTemplates();
}
