package com.grybisz.finances.templates.usecase;

import com.grybisz.finances.templates.domain.Template;
import com.grybisz.finances.templates.usecase.port.TemplateRepository;

public class CreateTemplate {

    private final TemplateRepository repository;

    public CreateTemplate(TemplateRepository repository) {
        this.repository = repository;
    }

    public String create(Template newTemplate) {
        return repository.saveTemplate(newTemplate);
    }
}
