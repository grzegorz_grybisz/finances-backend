package com.grybisz.finances.templates.usecase;

import com.grybisz.finances.templates.domain.Template;
import com.grybisz.finances.templates.usecase.port.TemplateRepository;

import java.util.Collection;
import java.util.Optional;

public class FindTemplate {

    private final TemplateRepository repository;

    public FindTemplate(TemplateRepository repository) {
        this.repository = repository;
    }

    public Optional<Template> findById(String templateId) {
        return repository.findTemplate(templateId);
    }

    public Collection<Template> findAll() {
        return repository.getAllTemplates();
    }
}
