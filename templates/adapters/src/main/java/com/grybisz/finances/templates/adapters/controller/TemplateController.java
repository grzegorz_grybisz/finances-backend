package com.grybisz.finances.templates.adapters.controller;

import com.grybisz.finances.templates.domain.Template;
import com.grybisz.finances.templates.usecase.CreateTemplate;
import com.grybisz.finances.templates.usecase.FindTemplate;

import java.util.Collection;
import java.util.Optional;

public class TemplateController {

    private CreateTemplate createTemplate;
    private FindTemplate findTemplate;

    public TemplateController(CreateTemplate createTemplate, FindTemplate findTemplate) {
        this.createTemplate = createTemplate;
        this.findTemplate = findTemplate;
    }

    public String createTemplate(Template template) {
        return createTemplate.create(template);
    }

    public Optional<Template> findTemplate(String templateId) {
        return findTemplate.findById(templateId);
    }


    public Collection<Template> getAllTemplates() {
        return findTemplate.findAll();
    }
}
