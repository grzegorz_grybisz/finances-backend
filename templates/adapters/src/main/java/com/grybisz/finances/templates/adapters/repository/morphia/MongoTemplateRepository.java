package com.grybisz.finances.templates.adapters.repository.morphia;

import com.grybisz.finances.templates.adapters.auth.UserContext;
import com.grybisz.finances.templates.domain.Template;
import com.grybisz.finances.templates.usecase.port.TemplateRepository;
import dev.morphia.Datastore;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

public class MongoTemplateRepository implements TemplateRepository {

    private final Logger logger = LoggerFactory.getLogger(MongoTemplateRepository.class);

    private final Datastore datastore;
    private final UserContext userContext;

    public MongoTemplateRepository(Datastore datastore, UserContext userContext) {
        this.datastore = datastore;
        this.userContext = userContext;
    }

    @Override
    public String saveTemplate(Template template) {
        TemplateEntity entity = TemplateEntity.from(template);
        entity.setOwner(userContext.getUserId());
        ObjectId objectId = (ObjectId) datastore.save(entity).getId();
        return objectId.toString();
    }

    @Override
    public Optional<Template> findTemplate(String templateId) {
        TemplateEntity entity = datastore.get(TemplateEntity.class, new ObjectId(templateId));
        return Optional.of(entity.toDomain());
    }

    @Override
    public Collection<Template> getAllTemplates() {
        logger.info("Get templates from MongoDB");
        return datastore.createQuery(TemplateEntity.class)
                .field("owner")
                .equal(userContext.getUserId())
                .asList()
                .stream()
                .map(TemplateEntity::toDomain)
                .collect(Collectors.toList());
    }
}
