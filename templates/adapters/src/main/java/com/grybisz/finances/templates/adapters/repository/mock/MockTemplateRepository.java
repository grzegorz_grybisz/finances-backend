package com.grybisz.finances.templates.adapters.repository.mock;

import com.grybisz.finances.templates.domain.Template;
import com.grybisz.finances.templates.domain.TransactionType;
import com.grybisz.finances.templates.usecase.port.TemplateRepository;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

public class MockTemplateRepository implements TemplateRepository {

    private Collection data;

    public MockTemplateRepository() {

        Template template1 = new Template();
        template1.setId("5c8ec26248025516d4b4c005");
        template1.setTemplateName("Mała kawa w pracy");
        template1.setName("Kawa");
        template1.setAmount(1.3);
        template1.setCategoryId(6194594L);
        template1.setType(TransactionType.EXPENSE);

        Template template2 = new Template();
        template2.setId("5c8ec26248025516d4b4c006");
        template2.setTemplateName("Duża kawa w pracy");
        template2.setName("Kawa");
        template2.setAmount(2.0);
        template2.setCategoryId(6194594L);
        template2.setType(TransactionType.EXPENSE);

        data = Arrays.asList(template1, template2);
    }

    @Override
    public String saveTemplate(Template template) {
        return null;
    }

    @Override
    public Optional<Template> findTemplate(String templateId) {
        return Optional.empty();
    }

    @Override
    public Collection<Template> getAllTemplates() {
        return data;
    }
}