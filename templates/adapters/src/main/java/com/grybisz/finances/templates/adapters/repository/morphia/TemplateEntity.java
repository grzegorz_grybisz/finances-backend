package com.grybisz.finances.templates.adapters.repository.morphia;

import com.grybisz.finances.templates.domain.Template;
import com.grybisz.finances.templates.domain.TransactionType;
import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Collection;

@Entity(value = "template", noClassnameStored = true)
public class TemplateEntity {

    @Id
    ObjectId id = new ObjectId();
    private String templateName;
    private String name;
    private Double amount;
    private TransactionType type;
    private Long categoryId;
    private Collection<String> tags;
    private String owner;

    private TemplateEntity() {
        tags = new ArrayList<>();
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public static TemplateEntity from(Template template) {
        TemplateEntity entity = new TemplateEntity();

        entity.templateName = template.getTemplateName();
        entity.name = template.getName();
        entity.amount = template.getAmount();
        entity.type = template.getType();
        entity.categoryId = template.getCategoryId();
        entity.tags = template.getTags();

        return entity;
    }

    public Template toDomain() {
        Template template = new Template();

        template.setName(this.name);
        template.setTemplateName(this.templateName);
        template.setAmount(this.amount);
        template.setType(this.type);
        template.setCategoryId(this.categoryId);
        template.setTags(this.tags);

        return template;
    }
}
