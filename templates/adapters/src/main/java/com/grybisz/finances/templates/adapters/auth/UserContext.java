package com.grybisz.finances.templates.adapters.auth;

import org.keycloak.representations.AccessToken;

public class UserContext {

    private AccessToken token;

    public UserContext(AccessToken token) {
        this.token = token;
    }

    public String getUserId() {
        return token.getSubject();
    }
}
