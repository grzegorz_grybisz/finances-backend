package com.grybisz.finances.templates.endpoints;

import com.grybisz.finances.templates.adapters.controller.TemplateController;
import com.grybisz.finances.templates.domain.Template;
import com.grybisz.finances.templates.dto.TemplateDto;
import com.grybisz.finances.templates.dto.TemplateResultDto;
import com.grybisz.finances.templates.dto.TemplateViewDto;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Optional;

import static com.grybisz.finances.templates.endpoints.TemplateDtoConverter.convertToDtos;
import static com.grybisz.finances.templates.endpoints.TemplateDtoConverter.convertToEntity;

/**
 * Created by Grzesiek on 2017-03-04.
 */
@RestController
@RequestMapping(value = "/templates", produces = "application/json")
@Api(tags = "Templates")
public class TemplateResource {

    private static Logger logger = LoggerFactory.getLogger(TemplateResource.class);


    TemplateController controller;

    @Autowired
    public TemplateResource(TemplateController controller) {
        this.controller = controller;
    }

    @CrossOrigin
    @PostMapping
    public TemplateResultDto createTemplate(@RequestBody TemplateDto templateDto) {
        logger.info("Create Template, body: {}", templateDto.toString());

        Template newTemplate = convertToEntity(templateDto);
        String id = controller.createTemplate(newTemplate);

        TemplateResultDto result = new TemplateResultDto();
        result.setId(id);
        return result;
    }

    @CrossOrigin
    @GetMapping(value = "/{templateId}")
    public TemplateViewDto getTemplate(@PathVariable String templateId) {
        logger.info("Find existing Expense, id:{}", templateId);

        Optional<Template> template = controller.findTemplate(templateId);

        return template.map(TemplateDtoConverter::convertToDto)
                .orElseThrow(() -> new IllegalArgumentException(
                        MessageFormat.format("Template with id: {0} does not exist", templateId)));
    }

    @CrossOrigin
    @GetMapping
    public Collection<TemplateViewDto> getAllTemplates() {
        logger.info("Get all Templates");

        Collection<Template> templates = controller.getAllTemplates();

        return convertToDtos(templates);
    }
}
