package com.grybisz.finances.templates;

import com.grybisz.finances.templates.adapters.auth.UserContext;
import com.grybisz.finances.templates.adapters.controller.TemplateController;
import com.grybisz.finances.templates.adapters.repository.morphia.MongoTemplateRepository;
import com.grybisz.finances.templates.adapters.repository.morphia.TemplateEntity;
import com.grybisz.finances.templates.usecase.CreateTemplate;
import com.grybisz.finances.templates.usecase.FindTemplate;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import dev.morphia.Datastore;
import dev.morphia.Morphia;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Grzesiek on 2017-02-26.
 */

@SpringBootApplication(exclude = {
        MongoAutoConfiguration.class,
        MongoDataAutoConfiguration.class
})
public class TemplatesApplication {

    @Value("${MONGODB_URI}")
    private String mongodbUri;

    public static void main(String[] args) {
        SpringApplication.run(TemplatesApplication.class, args);
    }

    @Bean
    @Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public TemplateController templateController() {

        MongoTemplateRepository repository = new MongoTemplateRepository(datastore(), userContext());

        return new TemplateController(
                new CreateTemplate(repository),
                new FindTemplate(repository)
        );
    }

    @Bean
    @Scope(scopeName = WebApplicationContext.SCOPE_APPLICATION)
    public Datastore datastore() {
        MongoClientURI parsedDbUri = new MongoClientURI(mongodbUri);
        MongoClient mongoClient = new MongoClient(parsedDbUri);

        Morphia morphia = new Morphia();
        morphia.mapPackageFromClass(TemplateEntity.class);
        Datastore datastore = morphia.createDatastore(mongoClient, parsedDbUri.getDatabase());
        datastore.ensureIndexes();
        return datastore;
    }


    @Bean
    @Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public UserContext userContext() {
        KeycloakPrincipal<KeycloakSecurityContext> principal =
                (KeycloakPrincipal<KeycloakSecurityContext>) getSecurityContext().getPrincipal();
        AccessToken token = principal.getKeycloakSecurityContext().getToken();

        return new UserContext(token);
    }

    @Bean
    @Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public KeycloakAuthenticationToken getSecurityContext() {
        return (KeycloakAuthenticationToken) getRequest().getUserPrincipal();
    }

    private HttpServletRequest getRequest() {
        ServletRequestAttributes requestAttributes =
                (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return requestAttributes.getRequest();
    }
}
