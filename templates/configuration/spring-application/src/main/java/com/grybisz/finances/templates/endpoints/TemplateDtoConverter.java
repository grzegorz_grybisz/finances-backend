package com.grybisz.finances.templates.endpoints;

import com.grybisz.finances.templates.domain.Template;
import com.grybisz.finances.templates.domain.TransactionType;
import com.grybisz.finances.templates.dto.TemplateDto;
import com.grybisz.finances.templates.dto.TemplateViewDto;
import com.grybisz.finances.templates.dto.TransactionDto;
import com.grybisz.finances.templates.dto.TransactionTypeDto;

import java.util.Collection;
import java.util.stream.Collectors;

public class TemplateDtoConverter {

    public static Collection<TemplateViewDto> convertToDtos(Collection<Template> entities) {
        return entities.stream()
                .map(TemplateDtoConverter::convertToDto)
                .collect(Collectors.toList());
    }

    public static TemplateViewDto convertToDto(Template entity) {

        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setName(entity.getName());
        transactionDto.setAmount(entity.getAmount());
        transactionDto.setCategory(String.valueOf(entity.getCategoryId()));
        transactionDto.setType(convertToDto(entity.getType()));
        transactionDto.setTags(entity.getTags());

        TemplateViewDto templateDto = new TemplateViewDto();

        templateDto.setId(entity.getId());
        templateDto.setName(entity.getTemplateName());
        templateDto.setTransaction(transactionDto);

        return templateDto;
    }

    private static TransactionTypeDto convertToDto(TransactionType type) {
        return TransactionTypeDto.valueOf(type.name());
    }

    public static Template convertToEntity(TemplateDto dto) {
        Template template = new Template();

        template.setTemplateName(dto.getName());
        template.setName(dto.getTransaction().getName());
        template.setAmount(dto.getTransaction().getAmount().orElse(null));
        template.setCategoryId(Long.valueOf(dto.getTransaction().getCategory()));
        template.setType(TransactionType.valueOf(dto.getTransaction().getType().name()));
        template.setTags(dto.getTransaction().getTags());

        return template;
    }
}
