package com.grybisz.finances.templates.endpoints;

import com.grybisz.finances.templates.domain.Template;
import com.grybisz.finances.templates.domain.TransactionType;
import com.grybisz.finances.templates.dto.TemplateDto;
import com.grybisz.finances.templates.dto.TemplateViewDto;
import com.grybisz.finances.templates.dto.TransactionDto;
import com.grybisz.finances.templates.dto.TransactionTypeDto;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Optional;

public class TemplateDtoConverterTest {

    @Test
    public void convertTemplateEntityToDto() {

        Template template = new Template();
        template.setId("5c8ec26248025516d4b4c005");
        template.setTemplateName("Coffee_Template");
        template.setName("Coffee");
        template.setAmount(1.3);
        template.setCategoryId(6194594L);
        template.setType(TransactionType.EXPENSE);
        template.setTags(Arrays.asList("tagA", "tagB"));

        TemplateViewDto dto = TemplateDtoConverter.convertToDto(template);

        Assert.assertThat(dto.getId(), Is.is("5c8ec26248025516d4b4c005"));
        Assert.assertThat(dto.getName(), Is.is("Coffee_Template"));
        Assert.assertThat(dto.getTransaction().getName(), Is.is("Coffee"));
        Assert.assertThat(dto.getTransaction().getAmount(), Is.is(Optional.of(1.3)));
        Assert.assertThat(dto.getTransaction().getCategory(), Is.is("6194594"));
        Assert.assertThat(dto.getTransaction().getType(), Is.is(TransactionTypeDto.EXPENSE));
        Assert.assertThat(dto.getTransaction().getTags(), Is.is(Arrays.asList("tagA", "tagB")));
    }

    @Test
    public void convertTemplateDtoToEntity() {

        TransactionDto transaction = new TransactionDto();
        transaction.setName("Coffee");
        transaction.setAmount(1.3);
        transaction.setCategory("6194594");
        transaction.setType(TransactionTypeDto.EXPENSE);
        transaction.setTags(Arrays.asList("tagA", "tagB"));

        TemplateDto dto = new TemplateDto();
        dto.setName("Coffee_Template");
        dto.setTransaction(transaction);

        Template entity = TemplateDtoConverter.convertToEntity(dto);

        Assert.assertThat(entity.getTemplateName(), Is.is("Coffee_Template"));
        Assert.assertThat(entity.getName(), Is.is("Coffee"));
        Assert.assertThat(entity.getAmount(), Is.is(1.3));
        Assert.assertThat(entity.getCategoryId(), Is.is(6194594L));
        Assert.assertThat(entity.getType(), Is.is(TransactionType.EXPENSE));
        Assert.assertThat(entity.getTags(), Is.is(Arrays.asList("tagA", "tagB")));
    }
}
