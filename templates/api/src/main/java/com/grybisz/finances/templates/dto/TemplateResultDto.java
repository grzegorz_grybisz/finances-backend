package com.grybisz.finances.templates.dto;

/**
 * Created by Grzesiek on 2017-03-04.
 */
public class TemplateResultDto {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
