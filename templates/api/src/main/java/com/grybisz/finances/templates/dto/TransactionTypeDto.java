package com.grybisz.finances.templates.dto;

/**
 * Created by Grzesiek on 2017-01-26.
 */
public enum TransactionTypeDto {
    EXPENSE, INCOME;
}
