package com.grybisz.finances.templates.dto;

import static com.grybisz.finances.templates.utility.EntityToJsonFormatter.toPrettyJsonString;

public class TemplateDto {

    private String name;
    private TransactionDto transaction;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TransactionDto getTransaction() {
        return transaction;
    }

    public void setTransaction(TransactionDto transaction) {
        this.transaction = transaction;
    }

    @Override
    public String toString() {
        return toPrettyJsonString(this);
    }
}
