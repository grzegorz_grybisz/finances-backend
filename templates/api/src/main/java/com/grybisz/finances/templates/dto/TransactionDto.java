package com.grybisz.finances.templates.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

public class TransactionDto {

    private String name;
    private Double amount;
    private String category;
    private TransactionTypeDto type;

    private Collection<String> tags = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Optional<Double> getAmount() {
        return Optional.ofNullable(amount);
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public TransactionTypeDto getType() {
        return type;
    }

    public void setType(TransactionTypeDto type) {
        this.type = type;
    }

    public Collection<String> getTags() {
        return tags;
    }

    public void setTags(Collection<String> tags) {
        this.tags = tags;
    }
}
