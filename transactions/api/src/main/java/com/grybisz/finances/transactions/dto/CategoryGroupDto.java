package com.grybisz.finances.transactions.dto;

import java.util.List;

public class CategoryGroupDto {

    private String id;
    private String name;
    private List<CategoryViewDto> categories;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CategoryViewDto> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryViewDto> categories) {
        this.categories = categories;
    }
}
