package com.grybisz.finances.transactions.dto;

public class TransactionInputDto extends TransactionDto {

    private String category;
    private Long wallet;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Long getWallet() {
        return wallet;
    }

    public void setWallet(Long wallet) {
        this.wallet = wallet;
    }
}
