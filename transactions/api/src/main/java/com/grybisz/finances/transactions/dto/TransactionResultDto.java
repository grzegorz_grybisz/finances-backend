package com.grybisz.finances.transactions.dto;

/**
 * Created by Grzesiek on 2016-11-26.
 */
public class TransactionResultDto {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
