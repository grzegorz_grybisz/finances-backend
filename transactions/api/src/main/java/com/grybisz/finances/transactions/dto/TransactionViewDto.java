package com.grybisz.finances.transactions.dto;

/**
 * Created by Grzesiek on 2016-11-26.
 */
public class TransactionViewDto extends TransactionDto {

    private String id;

    private CategoryViewDto category;
    private WalletOfTransactionViewDto wallet;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public CategoryViewDto getCategory() {
        return category;
    }

    public void setCategory(CategoryViewDto category) {
        this.category = category;
    }

    public WalletOfTransactionViewDto getWallet() {
        return wallet;
    }

    public void setWallet(WalletOfTransactionViewDto wallet) {
        this.wallet = wallet;
    }
}
