package com.grybisz.finances.transactions.dto;

import java.util.List;

import static com.grybisz.finances.utility.EntityToJsonFormatter.toPrettyJsonString;

/**
 * Created by Grzesiek on 2016-11-21.
 */
public abstract class TransactionDto {

    private String name;
    private double amount;
    private String date;
    private List<String> tags;

    private TransactionTypeDto type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public TransactionTypeDto getType() {
        return type;
    }

    public void setType(TransactionTypeDto type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return toPrettyJsonString(this);
    }
}