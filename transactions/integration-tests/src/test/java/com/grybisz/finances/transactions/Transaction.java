package com.grybisz.finances.transactions;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.Ignore;
import org.junit.runner.RunWith;

/**
 * Created by Grzesiek on 2016-11-12.
 */
@Ignore
@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/finances/transactions/features/")
public class Transaction {
}
