package com.grybisz.finances.transactions;

import com.grybisz.finances.transactions.datatypes.CreateTransactionTransformer;
import com.grybisz.finances.transactions.dto.TransactionDto;
import cucumber.api.TypeRegistry;
import cucumber.api.TypeRegistryConfigurer;
import io.cucumber.datatable.DataTableType;

import java.util.Locale;

public class TypeRegistryConfiguration implements TypeRegistryConfigurer {
    @Override
    public Locale locale() {
        return Locale.ENGLISH;
    }

    @Override
    public void configureTypeRegistry(TypeRegistry typeRegistry) {
        typeRegistry.defineDataTableType(new DataTableType(
                TransactionDto.class,
                new CreateTransactionTransformer()
        ));
    }
}
