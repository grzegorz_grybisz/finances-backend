package com.grybisz.finances.transactions.datatypes;

public class CreateTransactionEntity {

    private String name;
    private double amount;

    public CreateTransactionEntity(String name, double amount) {
        this.name = name;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public double getAmount() {
        return amount;
    }
}
