package com.grybisz.finances.transactions.steps;

import com.grybisz.finances.transactions.TransactionsApplication;
import com.grybisz.finances.transactions.datatypes.CreateTransactionEntity;
import com.grybisz.finances.transactions.dto.TransactionDto;
import com.grybisz.finances.transactions.dto.TransactionInputDto;
import com.grybisz.finances.transactions.dto.TransactionTypeDto;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import org.hamcrest.Matchers;
import org.junit.Ignore;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;

//TODO think about changing to groovy/spock
/**
 * Created by Grzesiek on 2016-11-21.
 */
@Ignore
@SpringBootTest(classes = TransactionsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration
public class TransactionSteps {

    private TransactionDto currentTransaction;
    //TODO change to selfURI? if test will return url without need to cut it
    private String expenseId;


    @Value("${local.server.port}")
    int port;

    @Before
    public void configurePorts() {
        RestAssured.port = port;
    }

    @Given("I made the following transaction")
    public void prepareExpenses(List<TransactionDto> expenses) {
        //CreateTransactionEntity transaction = expenses.get(0);
        currentTransaction = expenses.get(0);//convertToDto(transaction);
    }

    private TransactionDto convertToDto(CreateTransactionEntity transaction) {
        TransactionInputDto dto = new TransactionInputDto();
        dto.setName(transaction.getName());
        dto.setAmount(transaction.getAmount());
        dto.setType(TransactionTypeDto.EXPENSE);
        return dto;
    }

    @When("I add this transaction to my account")
    public void addExpenseToAccount() {
        expenseId = RestAssured.given().contentType("application/json")
                .body(currentTransaction)
                .post("/finances/transaction")
                .then().statusCode(200)
                .and().extract().jsonPath().getString("id");
    }

    @When("I change this transaction to")
    public void editExpenseInAccount(List<TransactionDto> expenses) {

        TransactionDto editedExpense = expenses.get(0);//convertToDto(expenses.get(0));
        RestAssured.given().contentType("application/json")
                .body(editedExpense)
                .put("/finances/transaction/" + expenseId)
                .then().statusCode(200);
        currentTransaction = editedExpense;
    }

    @Then("I should be able to get it from my account")
    public void getExpense() {

        //Cast to float, because restAssured only compares to float
        float newExpenseAmount = (float) (currentTransaction.getAmount());

        RestAssured.given().contentType("application/json")
                .get("/finances/transaction/{id}", expenseId)
                .then().statusCode(200)
                .and().body("name", Matchers.equalTo(currentTransaction.getName()))
                .and().body("amount", Matchers.equalTo(newExpenseAmount));

    }
}
