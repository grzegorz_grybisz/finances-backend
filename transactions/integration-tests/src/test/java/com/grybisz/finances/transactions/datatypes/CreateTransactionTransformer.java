package com.grybisz.finances.transactions.datatypes;

import com.grybisz.finances.transactions.dto.TransactionDto;
import com.grybisz.finances.transactions.dto.TransactionInputDto;
import com.grybisz.finances.transactions.dto.TransactionTypeDto;
import io.cucumber.datatable.TableEntryTransformer;

import java.util.Map;

public class CreateTransactionTransformer implements TableEntryTransformer<TransactionDto> {

    @Override
    public TransactionDto transform(Map<String, String> entry) {
        String transactionName = entry.get("name");
        Double transactionAmount = Double.parseDouble(entry.get("amount"));

        TransactionInputDto dto = new TransactionInputDto();
        dto.setName(transactionName);
        dto.setAmount(transactionAmount);
        dto.setType(TransactionTypeDto.EXPENSE);

        return dto;
    }
}
