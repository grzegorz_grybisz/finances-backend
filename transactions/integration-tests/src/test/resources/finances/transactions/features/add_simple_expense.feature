Feature: Adding new simple transaction
  In order to track my transactions
  As a user
  I want to be able to add new transactions to my account

  Scenario Outline: Adding transactions
    Given I made the following transaction
      | name              | amount |
      | <transactionName> |<transactionAmount>        |
    When I add this transaction to my account
    Then I should be able to get it from my account

    Examples: Expenses
      | transactionName      | transactionAmount|
      | Bread                | 3.5    |
      | Pepsi                | 2.20   |
      | A Bag of chips       | 5.32   |