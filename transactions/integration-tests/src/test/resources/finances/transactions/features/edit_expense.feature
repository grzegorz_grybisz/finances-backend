Feature: Editing transactions
  In order to manage my transactions and fix mistakes
  As a user
  I want to be able to edit transactions that i added to my account

  Scenario: Editing transaction
    Given I made the following transaction
      | name           | amount   |
      | Bread          | 2.3       |
    When I add this transaction to my account
    And I change this transaction to
      | name           | amount   |
      | Pepsi          | 3.4      |
    Then I should be able to get it from my account