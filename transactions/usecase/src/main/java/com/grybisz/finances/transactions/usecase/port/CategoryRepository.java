package com.grybisz.finances.transactions.usecase.port;

import com.grybisz.finances.transactions.domain.Category;
import com.grybisz.finances.transactions.domain.CategoryGroup;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository {

    Optional<Category> getCategory(Long id);

    List<CategoryGroup> getGroupedCategories();
}
