package com.grybisz.finances.transactions.usecase.port;

import com.grybisz.finances.transactions.domain.Wallet;

import java.util.List;
import java.util.Optional;

public interface WalletRepository {

    Optional<Wallet> getWallet(Long id);

    List<Wallet> getWallets();


}
