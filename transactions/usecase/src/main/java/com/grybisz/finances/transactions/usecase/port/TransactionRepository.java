package com.grybisz.finances.transactions.usecase.port;

import com.grybisz.finances.transactions.domain.Transaction;

import java.util.List;
import java.util.Optional;

/**
 * Created by Grzesiek on 2016-12-18.
 */
public interface TransactionRepository {

    String saveTransaction(Transaction transaction);

    void deleteTransaction(String id);

    Optional<Transaction> findTransaction(String id);

    List<Transaction> getTransactions(DateContext dateContext);

}
