package com.grybisz.finances.transactions.usecase.port;

import java.util.List;

public interface TagRepository {

    List<String> getTags();
}
