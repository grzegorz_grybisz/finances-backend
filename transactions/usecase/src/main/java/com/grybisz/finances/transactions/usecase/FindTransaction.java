package com.grybisz.finances.transactions.usecase;

import com.grybisz.finances.transactions.domain.Transaction;
import com.grybisz.finances.transactions.usecase.port.DateContext;
import com.grybisz.finances.transactions.usecase.port.TransactionRepository;

import java.util.Collection;
import java.util.Optional;

public class FindTransaction {

    private TransactionRepository repository;

    public FindTransaction(TransactionRepository repository) {
        this.repository = repository;
    }

    public Collection<Transaction> getTransactions(DateContext dateContext) {
        return repository.getTransactions(dateContext);
    }

    public Optional<Transaction> findTransaction(String id){
        return repository.findTransaction(id);
    }
}
