package com.grybisz.finances.transactions.usecase;

import com.grybisz.finances.transactions.domain.Transaction;
import com.grybisz.finances.transactions.usecase.port.TransactionRepository;

public class SaveTransaction {

    private final TransactionRepository repository;

    public SaveTransaction(TransactionRepository repository) {
        this.repository = repository;
    }

    public String saveTransaction(Transaction transaction) {
        return repository.saveTransaction(transaction);
    }
}
