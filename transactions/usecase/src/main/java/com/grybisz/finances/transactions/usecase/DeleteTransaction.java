package com.grybisz.finances.transactions.usecase;

import com.grybisz.finances.transactions.usecase.port.TransactionRepository;

public class DeleteTransaction {

    private TransactionRepository repository;

    public DeleteTransaction(TransactionRepository repository) {
        this.repository = repository;
    }

    public void deleteTransaction(String id){
        repository.deleteTransaction(id);
    }
}
