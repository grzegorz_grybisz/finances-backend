package com.grybisz.finances.transactions.usecase.port;

public interface DateContext {

    String getFormattedStartDate(String pattern);
    String getFormattedEndDate(String pattern);
}
