package com.grybisz.finances.transactions.repository

import com.grybisz.finances.transaction.adapters.repository.k4.RestTransactionRepository
import com.grybisz.finances.transaction.adapters.repository.k4.client.dto.MoneyTransactionViewDto
import spock.lang.Ignore
import spock.lang.Specification

/**
 * Created by Grzesiek on 2017-01-26.
 */
@Ignore
class RestTransactionRepositorySpecification extends Specification{

    def getTransactionById = GroovyMock(FinancesClient.GetTransactionByIdBuilder)
    def getTransactionBuilder = GroovyMock(FinancesClient.GetTransactionBuilder)
    def restClient = GroovyMock(FinancesClient)

    def repository = new RestTransactionRepository(restClient)

    def "save null expense"() {

        when:


        repository.saveTransaction(null)

        then:
        thrown(NullPointerException)
    }

    def "find expense transaction and get it's amount"(){

        given:
        def transactionView = new MoneyTransactionViewDto()
        transactionView.setAmount(-10)

        getTransactionById.get() >> transactionView
        //TODO should be string or int or long?
        getTransactionBuilder.byId(_ as Long) >> getTransactionById
        restClient.findTransaction() >> getTransactionBuilder

        when:
        def result = repository.findTransaction("123")

        then:
        result.isPresent()
        result.get().getAmount() == 10


    }
}
