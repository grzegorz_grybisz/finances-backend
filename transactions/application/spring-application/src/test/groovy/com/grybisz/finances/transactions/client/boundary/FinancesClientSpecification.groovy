package com.grybisz.finances.transactions.client.boundary

import com.grybisz.finances.transaction.adapters.repository.k4.client.FinancesClient
import com.grybisz.finances.transactions.TransactionsApplication
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.junit.Ignore
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.client.MockRestServiceServer
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

import java.time.LocalDate

import static org.springframework.test.web.client.match.MockRestRequestMatchers.content
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess

@Ignore
@SpringBootTest(classes = TransactionsApplication.class)
@ContextConfiguration
class FinancesClientSpecification extends Specification {

    public static final String BACKEND_URL = "https://secure.kontomierz.pl/k4/money_transactions"

    def jsonSlurper = new JsonSlurper()
    def jsonOutput = new JsonOutput()

    @Autowired
    RestTemplate restTemplate

    @Autowired
    FinancesClient client

    MockRestServiceServer mockRemoteResource

    void setup() {
        mockRemoteResource = MockRestServiceServer.createServer(restTemplate)
    }

    def "get transaction by id"() {

        given:
        def jsonFile = this.getClass().getResource("/client/json/expected-money-transaction.json")
        def expectedTransaction = jsonSlurper.parse(jsonFile)
        def result = jsonOutput.toJson(expectedTransaction)

        mockRemoteResource
                .expect(requestTo(BACKEND_URL +
                "/121889364.json?api_key=zZDC7Vv4vBaXOfxNNkNbuZiJ39oZ6caH2414s0uLFZ7jNjnDo4QgLme6KUxX2CL2"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(result, MediaType.APPLICATION_JSON))

        when:
        def transaction = client.findTransaction().byId(121889364).get()

        then:
        mockRemoteResource.verify()
        transaction.getId() == expectedTransaction.money_transaction.id
        transaction.getDescription() == expectedTransaction.money_transaction.description
        transaction.getAmount() == Double.parseDouble(expectedTransaction.money_transaction.amount)
    }

    def "get all transactions"() {

        given:
        def jsonFile = this.getClass().getResource("/client/json/transaction-get-all-expected-result.json")
        def expectedTransactions = jsonSlurper.parse(jsonFile)
        def result = jsonOutput.toJson(expectedTransactions)

        mockRemoteResource
                .expect(requestTo(BACKEND_URL +
                ".json?api_key=zZDC7Vv4vBaXOfxNNkNbuZiJ39oZ6caH2414s0uLFZ7jNjnDo4QgLme6KUxX2CL2"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(result, MediaType.APPLICATION_JSON))

        when:
        def transactions = client.findTransaction().all().get()

        then:
        mockRemoteResource.verify()
        transactions.size() == 2
    }

    def "create new transaction"() {

        given:
        def jsonFile = this.getClass().getResource("/client/json/money-transaction-post-expected-body.json")
        def expectedBody = jsonOutput.toJson(jsonSlurper.parse(jsonFile))

        def responseFile = this.getClass().getResource("/client/json/money-transaction-post-expected-response.json")
        def response = jsonOutput.toJson(jsonSlurper.parse(responseFile))

        mockRemoteResource.expect(requestTo(BACKEND_URL +
                ".json?api_key=zZDC7Vv4vBaXOfxNNkNbuZiJ39oZ6caH2414s0uLFZ7jNjnDo4QgLme6KUxX2CL2"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        //TODO think about eliminating matcher
                .andExpect(content().string(Matchers.bodyMatchesTo(expectedBody)))
                .andRespond(withSuccess(response, MediaType.APPLICATION_JSON_UTF8))

        when:
        client.newTransaction().withdrawal()
                .amount(2.8)
                .name("Bread")
                .clientAssignedId(Integer.toString(1234))
                .date(LocalDate.of(2017, 1, 15))
                .save()

        then:
        mockRemoteResource.verify()
    }

    def "edit existing transaction by id"() {

        given:
        def jsonFile = this.getClass().getResource("/client/json/money-transaction-put-expected-body.json")
        def expectedBody = jsonOutput.toJson(jsonSlurper.parse(jsonFile))

        def responseFile = this.getClass().getResource("/client/json/money-transaction-put-expected-response.json")
        def response = jsonOutput.toJson(jsonSlurper.parse(responseFile))

        mockRemoteResource.expect(requestTo(BACKEND_URL
                + "/10"
                + ".json?api_key=zZDC7Vv4vBaXOfxNNkNbuZiJ39oZ6caH2414s0uLFZ7jNjnDo4QgLme6KUxX2CL2"))
                .andExpect(method(HttpMethod.PUT))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        //TODO think about eliminating matcher
                .andExpect(content().string(Matchers.bodyMatchesTo(expectedBody)))
                .andRespond(withSuccess(response, MediaType.APPLICATION_JSON))

        when:
        client.updateTransaction(10).amount(2.8).name("Bread").save()

        then:
        mockRemoteResource.verify()
    }
}