package com.grybisz.finances.transactions.client.boundary;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.grybisz.finances.transaction.adapters.repository.k4.client.dto.MoneyTransactionWrapperDto;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by Grzesiek on 2017-01-15.
 */
public final class Matchers {

    private static final Logger logger = LoggerFactory.getLogger(Matchers.class);

    public static Matcher<String> bodyMatchesTo(String expectedBody) {
        return new BaseMatcher<String>() {
            @Override
            public boolean matches(Object item) {
                try {
                    String actualBody = (String) item;
                    ObjectMapper jsonObjectMapper = new ObjectMapper();

                    logger.debug("Compare json:" + actualBody + " to " + expectedBody);

                    Object expectedTransaction = jsonObjectMapper
                            .readValue(expectedBody, MoneyTransactionWrapperDto.class)
                            .getMoneyTransaction();

                    Object actualTransaction = jsonObjectMapper
                            .readValue(actualBody, MoneyTransactionWrapperDto.class)
                            .getMoneyTransaction();

                    return expectedTransaction.equals(actualTransaction);
                } catch (IOException e) {
                    // TODO: 2017-01-15 Handle in a better way 
                    throw new RuntimeException(e);
                }
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Body does not match");
            }
        };
    }
}
