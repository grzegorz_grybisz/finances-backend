package com.grybisz.finances.transactions.infrastructure.context;

import com.grybisz.finances.transactions.endpoints.SpringDateContext;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

public class SpringDateContextTest {

    @Test
    public void shouldFormatDateAccordingToPattern() {
        SpringDateContext springDateContext = new SpringDateContext();
        springDateContext.setStartDate(LocalDate.of(2011, 11, 1));
        springDateContext.setEndDate(LocalDate.of(2012, 12, 2));

        String formattedStartDate = springDateContext.getFormattedStartDate("dd-MM-yyyy");
        String formattedEndDate = springDateContext.getFormattedEndDate("dd-MM-yyyy");

        Assert.assertThat(formattedStartDate, Is.is("01-11-2011"));
        Assert.assertThat(formattedEndDate, Is.is("02-12-2012"));
    }
}
