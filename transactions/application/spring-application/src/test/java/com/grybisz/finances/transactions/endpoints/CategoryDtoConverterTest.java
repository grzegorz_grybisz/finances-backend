package com.grybisz.finances.transactions.endpoints;

import com.grybisz.finances.transactions.domain.Category;
import com.grybisz.finances.transactions.domain.CategoryGroup;
import com.grybisz.finances.transactions.dto.CategoryGroupDto;
import com.grybisz.finances.transactions.dto.CategoryViewDto;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;


public class CategoryDtoConverterTest {

    @Test
    public void convertCategoryToDto() {
        Category category = new Category(String.valueOf(1), "Food");

        CategoryViewDto dto = CategoryDtoConverter.convertToDto(category);

        assertThat(dto.getId(), is(String.valueOf(1)));
        assertThat(dto.getName(), is(String.valueOf("Food")));
    }

    @Test
    public void convertCategoryGroupToDto() {
        Category food = new Category(String.valueOf(1), "Food");
        Category clothes = new Category(String.valueOf(2), "Clothes");
        CategoryGroup shopping = new CategoryGroup(String.valueOf(100), "Shopping", Arrays.asList(food, clothes));

        CategoryGroupDto dto = CategoryDtoConverter.convertToDto(shopping);

        assertThat(dto.getId(), is(String.valueOf(100)));
        assertThat(dto.getName(), is("Shopping"));
        assertThat(dto.getCategories().size(), is(2));
    }

    @Test
    public void convertCategoryGroupWithNoCategoriesToDto() {
        CategoryGroup shopping = new CategoryGroup(String.valueOf(100), "Shopping", Collections.emptyList());

        CategoryGroupDto dto = CategoryDtoConverter.convertToDto(shopping);

        assertThat(dto.getId(), is(String.valueOf(100)));
        assertThat(dto.getName(), is("Shopping"));
        assertThat(dto.getCategories().isEmpty(), is(true));
    }
}
