package com.grybisz.finances.transactions.endpoints;

import com.grybisz.finances.transactions.domain.Category;
import com.grybisz.finances.transactions.domain.CategoryId;
import com.grybisz.finances.transactions.domain.Transaction;
import com.grybisz.finances.transactions.domain.TransactionId;
import com.grybisz.finances.transactions.domain.TransactionType;
import com.grybisz.finances.transactions.domain.Wallet;
import com.grybisz.finances.transactions.dto.TransactionInputDto;
import com.grybisz.finances.transactions.dto.TransactionTypeDto;
import com.grybisz.finances.transactions.dto.TransactionViewDto;
import org.junit.Test;

import java.time.LocalDate;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class TransactionDtoConverterTest {

    @Test
    public void convertTransactionToDto() {
        Transaction transaction = new Transaction();
        transaction.setId(TransactionId.from("1"));
        transaction.setName("Bread");
        transaction.setAmount(10.1);
        transaction.setType(TransactionType.EXPENSE);
        transaction.setDate(LocalDate.of(2010, 10, 20));
        Category category = new Category(String.valueOf(100), "Food");
        Wallet wallet = new Wallet();

        TransactionViewDto dto = TransactionDtoConverter.convertToDto(transaction, category, wallet);

        assertThat(dto.getId(), is("1"));
        assertThat(dto.getName(), is("Bread"));
        assertThat(dto.getAmount(), is(10.1));
        assertThat(dto.getType(), is(TransactionTypeDto.EXPENSE));
        assertThat(dto.getDate(), is("2010-10-20"));
        assertThat(dto.getCategory().getId(), is(String.valueOf(100)));
        assertThat(dto.getCategory().getName(), is(String.valueOf("Food")));
    }

    @Test
    public void convertCreateDtoToTransactionEntity() {
        TransactionInputDto dto = new TransactionInputDto();
        dto.setName("Ham");
        dto.setAmount(10.2);
        dto.setDate("2011-11-21");
        dto.setCategory(String.valueOf(100));
        dto.setType(TransactionTypeDto.INCOME);

        Transaction transaction = TransactionDtoConverter.convertToEntity(dto);

        assertThat(transaction.getId().value().isPresent(), is(false));
        assertThat(transaction.getName(), is("Ham"));
        assertThat(transaction.getAmount(), is(10.2));
        assertThat(transaction.getDate(), is(LocalDate.of(2011, 11, 21)));
        assertThat(transaction.getCategory(), is(CategoryId.from(100L)));
        assertThat(transaction.getType(), is(TransactionType.INCOME));
    }

    @Test
    public void convertUpdateDtoToTransactionEntity() {
        TransactionInputDto dto = new TransactionInputDto();
        dto.setName("Ham");
        dto.setAmount(10.2);
        dto.setDate("2011-11-21");
        dto.setCategory(String.valueOf(100));
        dto.setType(TransactionTypeDto.INCOME);

        Transaction transaction = TransactionDtoConverter.convertToEntity("1", dto);

        assertThat(transaction.getId(), is(TransactionId.from("1")));
        assertThat(transaction.getName(), is("Ham"));
        assertThat(transaction.getAmount(), is(10.2));
        assertThat(transaction.getDate(), is(LocalDate.of(2011, 11, 21)));
        assertThat(transaction.getCategory(), is(CategoryId.from(100L)));
        assertThat(transaction.getType(), is(TransactionType.INCOME));
    }
}
