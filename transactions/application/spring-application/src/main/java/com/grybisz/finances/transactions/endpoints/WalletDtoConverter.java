package com.grybisz.finances.transactions.endpoints;

import com.grybisz.finances.transactions.domain.Wallet;
import com.grybisz.finances.transactions.dto.WalletDto;

public class WalletDtoConverter {

    public static WalletDto convertToDto(Wallet wallet) {
        WalletDto dto = new WalletDto();

        dto.setId(wallet.getId());
        dto.setName(wallet.getName());
        dto.setAmount(wallet.getAmount());

        return dto;
    }
}
