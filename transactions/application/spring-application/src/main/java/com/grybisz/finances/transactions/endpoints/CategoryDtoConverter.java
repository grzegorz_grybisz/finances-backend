package com.grybisz.finances.transactions.endpoints;

import com.grybisz.finances.transactions.domain.Category;
import com.grybisz.finances.transactions.domain.CategoryGroup;
import com.grybisz.finances.transactions.dto.CategoryGroupDto;
import com.grybisz.finances.transactions.dto.CategoryViewDto;

import java.util.List;
import java.util.stream.Collectors;

public class CategoryDtoConverter {

    public static CategoryViewDto convertToDto(Category category) {
        CategoryViewDto dto = new CategoryViewDto();
        dto.setId(category.getId());
        dto.setName(category.getName());
        return dto;
    }

    public static CategoryGroupDto convertToDto(CategoryGroup group) {
        CategoryGroupDto dto = new CategoryGroupDto();
        dto.setId(group.getId());
        dto.setName(group.getName());


        List<CategoryViewDto> categoriesDtos = convertCategoriesToEntities(group.getCategories());
        dto.setCategories(categoriesDtos);

        return dto;
    }

    private static List<CategoryViewDto> convertCategoriesToEntities(List<Category> categories) {
        return categories
                .stream()
                .map(CategoryDtoConverter::convertToDto)
                .collect(Collectors.toList());
    }
}
