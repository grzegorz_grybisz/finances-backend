package com.grybisz.finances.transactions.endpoints;

import com.grybisz.finances.transactions.usecase.port.TagRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/transaction/tag")
@Api(tags = "Tags")
public class TagResource {

    private static Logger logger = LoggerFactory.getLogger(TagResource.class);

    @Autowired
    TagRepository tagRepository;

    @CrossOrigin
    @GetMapping
    @ApiOperation(value = "Get all Tags")
    public List<String> getAllTags() {
        logger.info("Get all Tags");
        return tagRepository.getTags();
    }

}
