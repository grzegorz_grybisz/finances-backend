package com.grybisz.finances.transactions.endpoints;

import com.grybisz.finances.transactions.usecase.port.DateContext;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class SpringDateContext implements DateContext {

    LocalDate startDate;
    LocalDate endDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public String getFormattedStartDate(String pattern) {
        return getFormattedDate(startDate, pattern);
    }

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public String getFormattedEndDate(String pattern) {
        return getFormattedDate(endDate, pattern);
    }

    private String getFormattedDate(LocalDate date, String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return date.format(formatter);
    }

    @Override
    public String toString() {
        return "DateContext: From " + startDate.toString() + " To " + endDate.toString();
    }
}
