package com.grybisz.finances.transactions.endpoints;


import com.grybisz.finances.transactions.domain.Category;
import com.grybisz.finances.transactions.domain.CategoryId;
import com.grybisz.finances.transactions.domain.Transaction;
import com.grybisz.finances.transactions.domain.TransactionId;
import com.grybisz.finances.transactions.domain.TransactionType;
import com.grybisz.finances.transactions.domain.Wallet;
import com.grybisz.finances.transactions.dto.CategoryViewDto;
import com.grybisz.finances.transactions.dto.TransactionInputDto;
import com.grybisz.finances.transactions.dto.TransactionTypeDto;
import com.grybisz.finances.transactions.dto.TransactionViewDto;
import com.grybisz.finances.transactions.dto.WalletOfTransactionViewDto;

import java.time.LocalDate;

public class TransactionDtoConverter {
    public static TransactionViewDto convertToDto(Transaction transaction, Category category, Wallet wallet) {
        TransactionViewDto dto = new TransactionViewDto();

        String id = transaction.getId().value().orElse("");
        dto.setId(id);

        dto.setName(transaction.getName());
        dto.setAmount(transaction.getAmount());
        dto.setDate(transaction.getDate().toString());
        dto.setTags(transaction.getTags());

        TransactionTypeDto type = convertToDto(transaction.getType());
        dto.setType(type);

        CategoryViewDto categoryDto = CategoryDtoConverter.convertToDto(category);
        dto.setCategory(categoryDto);

        WalletOfTransactionViewDto walletDto = convertToDto(wallet);
        dto.setWallet(walletDto);

        return dto;
    }

    private static WalletOfTransactionViewDto convertToDto(Wallet wallet) {
        WalletOfTransactionViewDto dto = new WalletOfTransactionViewDto();

        dto.setId(wallet.getId());
        dto.setName(wallet.getName());

        return dto;
    }

    private static TransactionTypeDto convertToDto(TransactionType type) {
        return TransactionTypeDto.valueOf(type.name());
    }

    public static Transaction convertToEntity(TransactionInputDto dto) {
        Transaction transaction = new Transaction();

        transaction.setName(dto.getName());
        transaction.setAmount(dto.getAmount());
        transaction.setType(convertToEntity(dto.getType()));
        transaction.setDate(LocalDate.parse(dto.getDate()));
        transaction.setTags(dto.getTags());

        CategoryId categoryId = CategoryId.from(dto.getCategory());
        transaction.setCategory(categoryId);

        transaction.setWallet(dto.getWallet());

        return transaction;
    }

    private static TransactionType convertToEntity(TransactionTypeDto type) {
        return TransactionType.valueOf(type.name());
    }

    public static Transaction convertToEntity(String id, TransactionInputDto dto) {
        Transaction transaction = convertToEntity(dto);
        transaction.setId(TransactionId.from(id));
        return transaction;
    }
}
