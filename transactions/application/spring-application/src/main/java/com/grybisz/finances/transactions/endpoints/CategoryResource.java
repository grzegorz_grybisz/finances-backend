package com.grybisz.finances.transactions.endpoints;

import com.grybisz.finances.transactions.domain.Category;
import com.grybisz.finances.transactions.domain.CategoryGroup;
import com.grybisz.finances.transactions.dto.CategoryGroupDto;
import com.grybisz.finances.transactions.dto.CategoryViewDto;
import com.grybisz.finances.transactions.usecase.port.CategoryRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/transaction/category")
@Api(tags = "Categories")
public class CategoryResource {

    private static Logger logger = LoggerFactory.getLogger(CategoryResource.class);

    @Autowired
    CategoryRepository categoryRepository;

    @CrossOrigin
    @GetMapping(value = "/{categoryId}")
    @ApiOperation("Get Category by ID")
    public CategoryViewDto getCategory(@PathVariable String categoryId) {
        logger.info("Find existing category, id:{}", categoryId);

        Optional<Category> category = categoryRepository.getCategory(Long.valueOf(categoryId));
        return category.map(CategoryDtoConverter::convertToDto)
                .orElseThrow(() -> new IllegalArgumentException("Cannot find category with " + categoryId));
    }

    @CrossOrigin
    @GetMapping
    @ApiOperation(value = "Get all Categories")
    public List<CategoryGroupDto> getAllCategories() {
        logger.info("Get all Categories");

        List<CategoryGroup> categories = categoryRepository.getGroupedCategories();

        return categories.stream()
                .map(CategoryDtoConverter::convertToDto)
                .collect(Collectors.toList());
    }
}