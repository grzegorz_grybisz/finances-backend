package com.grybisz.finances.transactions.infrastructure.auth;

import com.grybisz.finances.transaction.adapters.repository.k4.client.FinancesContext;

import java.util.Collections;
import java.util.Map;

public class UserContext implements FinancesContext {

    private Map<String, Object> claims;

    public UserContext(Map<String, Object> claims) {
        this.claims = Collections.unmodifiableMap(claims);
    }

    @Override
    public String getFinancesKey() {
        if (claims.containsKey("finances")) {
            return (String) claims.get("finances");
        }
        throw new IllegalStateException("Finances Key is not available");
    }
}
