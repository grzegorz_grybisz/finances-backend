package com.grybisz.finances.transactions.endpoints;

import com.grybisz.finances.transactions.domain.Wallet;
import com.grybisz.finances.transactions.usecase.port.WalletRepository;
import com.grybisz.finances.transactions.dto.WalletDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping(value = "/transaction/wallet", produces = "application/json")
@Api(tags = "Wallets")
public class WalletResource {

    private static Logger logger = LoggerFactory.getLogger(WalletResource.class);

    @Autowired
    WalletRepository walletRepository;

    @CrossOrigin
    @GetMapping
    @ApiOperation(value = "Get all Wallets")
    public List<WalletDto> getAllWallets() {
        logger.info("Get all wallets");

        List<Wallet> categories = walletRepository.getWallets();

        return categories.stream()
                .map(WalletDtoConverter::convertToDto)
                .collect(toList());
    }
}
