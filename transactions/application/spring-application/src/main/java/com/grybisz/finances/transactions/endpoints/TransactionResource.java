package com.grybisz.finances.transactions.endpoints;

import com.grybisz.finances.transactions.domain.Category;
import com.grybisz.finances.transactions.domain.CategoryId;
import com.grybisz.finances.transactions.domain.Transaction;
import com.grybisz.finances.transactions.domain.Wallet;
import com.grybisz.finances.transactions.dto.TransactionInputDto;
import com.grybisz.finances.transactions.dto.TransactionResultDto;
import com.grybisz.finances.transactions.dto.TransactionViewDto;
import com.grybisz.finances.transactions.usecase.DeleteTransaction;
import com.grybisz.finances.transactions.usecase.FindTransaction;
import com.grybisz.finances.transactions.usecase.SaveTransaction;
import com.grybisz.finances.transactions.usecase.port.CategoryRepository;
import com.grybisz.finances.transactions.usecase.port.WalletRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Created by Grzesiek on 2016-11-26.
 */
@RestController
@RequestMapping(value = "/transaction", produces = "application/json")
@Api(tags = "Transactions")
public class TransactionResource {

    private static Logger logger = LoggerFactory.getLogger(TransactionResource.class);

    SaveTransaction saveTransaction;
    FindTransaction findTransaction;
    DeleteTransaction deleteTransaction;
    CategoryRepository categoryRepository;
    WalletRepository walletRepository;

    @Autowired
    public TransactionResource(SaveTransaction saveTransaction,
                               FindTransaction findTransaction,
                               DeleteTransaction deleteTransaction,
                               CategoryRepository categoryRepository,
                               WalletRepository walletRepository) {
        this.saveTransaction = saveTransaction;
        this.findTransaction = findTransaction;
        this.deleteTransaction = deleteTransaction;
        this.categoryRepository = categoryRepository;
        this.walletRepository = walletRepository;
    }

    @CrossOrigin
    @PostMapping
    @ApiOperation(value = "Create new Transaction", code = 201, response = TransactionResultDto.class)
    public ResponseEntity create(@RequestBody TransactionInputDto transactionDto) {
        try {
            logger.info("Create Transaction, body: {}", transactionDto.toString());
            return createTransaction(transactionDto);
        } catch (Exception e) {
            logger.error("Exception: ", e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private ResponseEntity createTransaction(TransactionInputDto transactionDto) {
        Transaction newTransaction = TransactionDtoConverter.convertToEntity(transactionDto);
        String id = saveTransaction.saveTransaction(newTransaction);

        TransactionResultDto result = new TransactionResultDto();
        result.setId(id);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @CrossOrigin
    @PutMapping(value = "/{transactionId}")
    @ApiOperation(value = "Update existing Transaction")
    public TransactionResultDto editTransaction(@PathVariable String transactionId,
                                                @RequestBody TransactionInputDto transactionDto) {
        logger.info("Edit existing Transaction, id:{} body:{}", transactionId, transactionDto.toString());

        Transaction editedTransaction = TransactionDtoConverter.convertToEntity(transactionId, transactionDto);
        String id = saveTransaction.saveTransaction(editedTransaction);

        TransactionResultDto result = new TransactionResultDto();
        result.setId(id);
        return result;
    }

    @CrossOrigin
    @GetMapping(value = "/{transactionId}")
    @ApiOperation(value = "Get Transaction by Id")
    public TransactionViewDto getTransaction(@PathVariable String transactionId) {
        logger.info("Find existing Transaction, id:{}", transactionId);

        //todo extract to use case
        Optional<Transaction> foundTransaction = findTransaction.findTransaction(transactionId);

        if (foundTransaction.isPresent()) {
            Transaction transaction = foundTransaction.get();

            Category category = getCategory(transaction);
            Wallet wallet = getWallet(transaction);

            return TransactionDtoConverter.convertToDto(transaction, category, wallet);
        }
        throw new IllegalArgumentException("No such Transaction");
    }

    @CrossOrigin
    @GetMapping
    @ApiOperation(value = "Get all Transactions")
    public List<TransactionViewDto> getAllTransactions(SpringDateContext springDateContext) {
        logger.info("Get all Transactions");
        logger.info("Date context: " + springDateContext.toString());

        Collection<Transaction> transactions = findTransaction.getTransactions(springDateContext);

        List<TransactionViewDto> viewDtos = new ArrayList<>();
        for (Transaction transaction : transactions) {
            Category category = getCategory(transaction);
            Wallet wallet = getWallet(transaction);

            viewDtos.add(TransactionDtoConverter.convertToDto(transaction, category, wallet));
        }
        logger.info("Response: :{}", viewDtos.size() + " transactions");
        return viewDtos;
    }

    private Category getCategory(Transaction transaction) {
        CategoryId categoryId = transaction.getCategory();
        return categoryRepository.getCategory(categoryId.value())
                .orElseThrow(() -> new IllegalArgumentException("Cannot find category of id: " + categoryId));
    }

    private Wallet getWallet(Transaction transaction) {
        Long walletId = transaction.getWallet();
        return walletRepository.getWallet(walletId)
                .orElseThrow(() -> new IllegalArgumentException("Cannot find wallet of id: " + walletId));
    }

    @CrossOrigin
    @DeleteMapping(value = "/{transactionId}")
    @ApiOperation(value = "Remove existing Transaction")
    public ResponseEntity deleteTransaction(@PathVariable String transactionId) {
        logger.info("Remove Transaction, id:{}", transactionId);

        deleteTransaction.deleteTransaction(transactionId);

        return ResponseEntity.noContent()
                .build();
    }
}