package com.grybisz.finances.transactions.config;

import com.grybisz.finances.transaction.adapters.repository.k4.RestTagRepository;
import com.grybisz.finances.transaction.adapters.repository.k4.RestTemplateCategoryRepository;
import com.grybisz.finances.transaction.adapters.repository.k4.RestTransactionRepository;
import com.grybisz.finances.transaction.adapters.repository.k4.RestWalletRepository;
import com.grybisz.finances.transaction.adapters.repository.k4.client.FinancesClient;
import com.grybisz.finances.transactions.infrastructure.auth.UserContext;
import com.grybisz.finances.transactions.usecase.DeleteTransaction;
import com.grybisz.finances.transactions.usecase.FindTransaction;
import com.grybisz.finances.transactions.usecase.SaveTransaction;
import com.grybisz.finances.transactions.usecase.port.CategoryRepository;
import com.grybisz.finances.transactions.usecase.port.TagRepository;
import com.grybisz.finances.transactions.usecase.port.WalletRepository;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.net.ssl.SSLContext;
import javax.servlet.http.HttpServletRequest;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Map;

@Configuration
public class BeansConfiguration {

    @Bean
    public RestTemplate restTemplate() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        // TODO: Think about UNWRAP_ROOT_VALUE

        TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

        SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
                .loadTrustMaterial(null, acceptingTrustStrategy)
                .build();

        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLSocketFactory(csf)
                .build();

        HttpComponentsClientHttpRequestFactory requestFactory =
                new HttpComponentsClientHttpRequestFactory();

        requestFactory.setHttpClient(httpClient);


        return new RestTemplate(requestFactory);
    }

    @Bean
    @Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public SaveTransaction saveTransaction()
            throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        return new SaveTransaction(K4TransactionRepository());
    }

    @Bean
    @Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public FindTransaction findTransaction()
            throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        return new FindTransaction(K4TransactionRepository());
    }

    @Bean
    @Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public DeleteTransaction deleteTransaction()
            throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        return new DeleteTransaction(K4TransactionRepository());
    }

    private RestTransactionRepository K4TransactionRepository() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        return new RestTransactionRepository(k4Client());
    }

    @Bean
    @Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public CategoryRepository categoryRepository()
            throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        return new RestTemplateCategoryRepository(k4Client());
    }

    @Bean
    @Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public WalletRepository walletRepository()
            throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        return new RestWalletRepository(k4Client());
    }

    @Bean
    @Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public TagRepository tagRepository()
            throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        return new RestTagRepository(k4Client());
    }

    @Bean
    @Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public UserContext userContext() {
        KeycloakPrincipal<KeycloakSecurityContext> principal =
                (KeycloakPrincipal<KeycloakSecurityContext>) getSecurityContext().getPrincipal();
        AccessToken token = principal.getKeycloakSecurityContext().getToken();

        Map<String, Object> otherClaims = token.getOtherClaims();

        return new UserContext(otherClaims);
    }

    @Bean
    @Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public KeycloakAuthenticationToken getSecurityContext() {
        return (KeycloakAuthenticationToken) getRequest().getUserPrincipal();
    }

    private FinancesClient k4Client() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        return new FinancesClient(restTemplate(), userContext());
    }

    private HttpServletRequest getRequest() {
        ServletRequestAttributes requestAttributes =
                (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return requestAttributes.getRequest();
    }

}
