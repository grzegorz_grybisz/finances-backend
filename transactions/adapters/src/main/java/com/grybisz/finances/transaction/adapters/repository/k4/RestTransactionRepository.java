package com.grybisz.finances.transaction.adapters.repository.k4;

import com.grybisz.finances.transaction.adapters.repository.k4.client.FinancesClient;
import com.grybisz.finances.transaction.adapters.repository.k4.client.dto.MoneyTransactionViewDto;
import com.grybisz.finances.transactions.domain.Category;
import com.grybisz.finances.transactions.domain.CategoryId;
import com.grybisz.finances.transactions.domain.Transaction;
import com.grybisz.finances.transactions.domain.TransactionId;
import com.grybisz.finances.transactions.domain.TransactionType;
import com.grybisz.finances.transactions.usecase.port.DateContext;
import com.grybisz.finances.transactions.usecase.port.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * Created by Grzesiek on 2017-01-15.
 */
public final class RestTransactionRepository implements TransactionRepository {

    private final Logger logger = LoggerFactory.getLogger(RestTransactionRepository.class);

    private final FinancesClient financesClient;

    @Autowired
    public RestTransactionRepository(FinancesClient financesClient) {
        this.financesClient = financesClient;
    }

    @Override
    public Optional<Transaction> findTransaction(String id) {
        MoneyTransactionViewDto moneyTransaction = financesClient.findTransaction()
                .byId(Long.parseLong(id))
                .get();
        logger.info("Received response:{}", moneyTransaction);

        Transaction transaction = convertToEntity(moneyTransaction);
        return Optional.of(transaction);
    }

    @Override
    public List<Transaction> getTransactions(DateContext dateContext) {
        List<MoneyTransactionViewDto> transactions = financesClient.findTransaction()
                .all()
                .in(dateContext)
                .get();
        logger.info("Received response:{}", transactions);

        return transactions.stream()
                .map(this::convertToEntity)
                .collect(toList());
    }

    private Transaction convertToEntity(MoneyTransactionViewDto transactionViewDto) {
        Transaction transaction = new Transaction();
        String id = String.valueOf(transactionViewDto.getId());
        double amount = calculateAmount(transactionViewDto);
        String description = transactionViewDto.getDescription();
        List<String> tags = parseTags(transactionViewDto.getTags());
        LocalDate date = LocalDate.parse(transactionViewDto.getDate());
        TransactionType transactionType = getTransactionType(transactionViewDto);
        long walletId = transactionViewDto.getWalletId();

        Category category = convertToCategoryEntity(transactionViewDto);

        transaction.setId(TransactionId.from(id));
        transaction.setAmount(amount);
        transaction.setName(description);
        transaction.setDate(date);
        transaction.setType(transactionType);
        transaction.setCategory(CategoryId.from(category.getId()));
        transaction.setWallet(walletId);
        transaction.setTags(tags);
        return transaction;
    }

    private List<String> parseTags(String tags) {
        String[] parsedTags = tags.split(",");

        return Stream.of(parsedTags)
                .map(String::trim)
                .filter(s -> !s.isEmpty())
                .collect(toList());
    }

    private Category convertToCategoryEntity(MoneyTransactionViewDto transactionViewDto) {
        String categoryId = String.valueOf(transactionViewDto.getCategoryId());
        String categoryName = transactionViewDto.getCategoryName();

        return new Category(categoryId, categoryName);
    }

    private double calculateAmount(MoneyTransactionViewDto transaction) {
        return Math.abs(transaction.getAmount());
    }

    private TransactionType getTransactionType(MoneyTransactionViewDto transaction) {
        if (transaction.getAmount() >= 0) {
            return TransactionType.INCOME;
        }
        return TransactionType.EXPENSE;
    }

    @Override
    public String saveTransaction(Transaction transaction) {
        Optional<String> transactionId = transaction.getId().value();

        MoneyTransactionViewDto viewDto;
        if (transactionId.isPresent()) {
            viewDto = updateExistingTransaction(transaction, transactionId.get());
        } else {
            viewDto = createNewTransaction(transaction);
        }

        logger.info("Received response:{}", viewDto);
        return String.valueOf(viewDto.getId());
    }

    @Override
    public void deleteTransaction(String id) {
        logger.info("REST Client, delete transaction {}", id);
        financesClient.deleteTransaction(Long.valueOf(id));
    }

    private MoneyTransactionViewDto updateExistingTransaction(Transaction transaction, String stringId) {
        long id = Long.parseLong(stringId);
        logger.info("REST Client, update transaction {}:{}", id, transaction);

        return financesClient.updateTransaction(id)
                .name(transaction.getName())
                .amount(transaction.getAmount())
                .date(transaction.getDate())
                .categoryId(transaction.getCategory())
                .tags(transaction.getTags())
                .walletId(transaction.getWallet())
                .save();
    }

    private MoneyTransactionViewDto createNewTransaction(Transaction transaction) {
        FinancesClient.PostTransactionBuilder builder;
        if (transaction.getType() == TransactionType.EXPENSE) {
            builder = financesClient.newTransaction().withdrawal();
        } else {
            builder = financesClient.newTransaction().deposit();
        }

        logger.info("REST Client, create new transaction:{}", transaction);

        return builder.name(transaction.getName())
                .amount(transaction.getAmount())
                .date(transaction.getDate())
                .categoryId(transaction.getCategory())
                .tags(transaction.getTags())
                .walletId(transaction.getWallet())
                .clientAssignedId(UUID.randomUUID().toString())
                .save();
    }
}
