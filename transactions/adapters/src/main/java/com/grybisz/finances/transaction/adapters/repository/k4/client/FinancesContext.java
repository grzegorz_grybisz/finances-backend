package com.grybisz.finances.transaction.adapters.repository.k4.client;

public interface FinancesContext {

    String getFinancesKey();
}
