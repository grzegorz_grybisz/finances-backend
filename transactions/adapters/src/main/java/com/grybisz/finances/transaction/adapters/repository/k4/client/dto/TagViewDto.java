package com.grybisz.finances.transaction.adapters.repository.k4.client.dto;

public class TagViewDto {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
