package com.grybisz.finances.transaction.adapters.repository.k4.client.dto;

import java.util.List;

public class TagsViewWrapperDto {

    private List<TagViewDto> tags;

    public List<TagViewDto> getTags() {
        return tags;
    }

    public void setTags(List<TagViewDto> tags) {
        this.tags = tags;
    }
}
