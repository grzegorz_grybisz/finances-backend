package com.grybisz.finances.transaction.adapters.repository.k4.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class FinancesCategoryGroupWrapperDto {

    private List<FinancesCategoryGroupDto> categoryGroups;

    @JsonProperty("category_groups")
    public List<FinancesCategoryGroupDto> getCategoryGroups() {
        return categoryGroups;
    }

    @JsonProperty("category_groups")
    public void setCategoryGroups(List<FinancesCategoryGroupDto> categoryGroups) {
        this.categoryGroups = categoryGroups;
    }
}
