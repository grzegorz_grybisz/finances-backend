package com.grybisz.finances.transaction.adapters.repository.k4.client.dto;

import java.util.List;

public class FinancesCategoryGroupDto {
    private Long id;
    private String name;
    private List<FinancesCategoryDto> categories;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FinancesCategoryDto> getCategories() {
        return categories;
    }

    public void setCategories(List<FinancesCategoryDto> categories) {
        this.categories = categories;
    }
}
