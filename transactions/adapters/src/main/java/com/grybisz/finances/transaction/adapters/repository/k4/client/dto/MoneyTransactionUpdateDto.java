package com.grybisz.finances.transaction.adapters.repository.k4.client.dto;


import com.grybisz.finances.transactions.domain.EntityToJsonFormatter;

/**
 * Created by Grzesiek on 2017-02-26.
 */
public final class MoneyTransactionUpdateDto extends MoneyTransactionInputDto {

    @Override
    public String toString() {
        return EntityToJsonFormatter.toPrettyJsonString(this);
    }

}
