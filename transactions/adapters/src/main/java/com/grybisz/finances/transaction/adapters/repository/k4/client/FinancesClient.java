package com.grybisz.finances.transaction.adapters.repository.k4.client;

import com.grybisz.finances.transaction.adapters.repository.k4.client.dto.FinancesCategoryGroupDto;
import com.grybisz.finances.transaction.adapters.repository.k4.client.dto.FinancesCategoryGroupWrapperDto;
import com.grybisz.finances.transaction.adapters.repository.k4.client.dto.FinancesUserAccountDto;
import com.grybisz.finances.transaction.adapters.repository.k4.client.dto.FinancesUserAccountWrapperDto;
import com.grybisz.finances.transaction.adapters.repository.k4.client.dto.MoneyTransactionCreateDto;
import com.grybisz.finances.transaction.adapters.repository.k4.client.dto.MoneyTransactionUpdateDto;
import com.grybisz.finances.transaction.adapters.repository.k4.client.dto.MoneyTransactionViewDto;
import com.grybisz.finances.transaction.adapters.repository.k4.client.dto.MoneyTransactionViewWrapperDto;
import com.grybisz.finances.transaction.adapters.repository.k4.client.dto.MoneyTransactionWrapperDto;
import com.grybisz.finances.transaction.adapters.repository.k4.client.dto.TagViewDto;
import com.grybisz.finances.transaction.adapters.repository.k4.client.dto.TagsViewWrapperDto;
import com.grybisz.finances.transactions.domain.CategoryId;
import com.grybisz.finances.transactions.usecase.port.DateContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Grzesiek on 2016-12-25.
 */
//TODO make final if allowed by tests
public class FinancesClient {

    private final Logger logger = LoggerFactory.getLogger(FinancesClient.class);

    // TODO: extract to some properties?
    private static final String BACKEND_URL = "https://secure.kontomierz.pl/k4/";
    private static final String FORMAT = ".json";

    private RestTemplate restTemplate;
    private FinancesContext financesContext;

    @Autowired
    public FinancesClient(RestTemplate restTemplate, FinancesContext financesContext) {
        this.restTemplate = restTemplate;
        this.financesContext = financesContext;
    }

    public List<TagViewDto> getTags() {
        URI backedUri = UriComponentsBuilder.fromUriString(FinancesClient.BACKEND_URL)
                .path("tags")
                .path(FinancesClient.FORMAT)
                .queryParam("api_key", financesContext.getFinancesKey())
                .build()
                .toUri();

        TagsViewWrapperDto wrapper = restTemplate.getForObject(backedUri, TagsViewWrapperDto.class);
        return wrapper.getTags();
    }

    public List<FinancesCategoryGroupDto> getCategories() {
        URI backendUri = UriComponentsBuilder.fromUriString(FinancesClient.BACKEND_URL)
                .path("categories")
                .path(FinancesClient.FORMAT)
                .queryParam("in_wallet", true)
                .queryParam("api_key", financesContext.getFinancesKey())
                .build()
                .toUri();

        FinancesCategoryGroupWrapperDto transactionWrapper = restTemplate
                .getForObject(backendUri, FinancesCategoryGroupWrapperDto.class);
        return transactionWrapper.getCategoryGroups();
    }

    public List<FinancesUserAccountDto> getWallets() {
        URI backendUri = UriComponentsBuilder.fromUriString(FinancesClient.BACKEND_URL)
                .path("user_accounts")
                .path(FinancesClient.FORMAT)
                .queryParam("api_key", financesContext.getFinancesKey())
                .build()
                .toUri();

        ResponseEntity<List<FinancesUserAccountWrapperDto>> response = restTemplate.exchange(
                backendUri,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<FinancesUserAccountWrapperDto>>() {
                });

        return response.getBody()
                .stream()
                .map(FinancesUserAccountWrapperDto::getUserAccount)
                .collect(Collectors.toList());
    }

    public void deleteTransaction(long id) {
        URI backendUri = UriComponentsBuilder.fromUriString(FinancesClient.BACKEND_URL)
                .path("money_transactions/")
                .path(String.valueOf(id))
                .path(FinancesClient.FORMAT)
                .queryParam("api_key", financesContext.getFinancesKey())
                .build()
                .toUri();

        restTemplate.delete(backendUri);
    }

    public GetTransactionBuilder findTransaction() {
        return new GetTransactionBuilder();
    }

    public PostTransactionDirectionBuilder newTransaction() {
        return new PostTransactionDirectionBuilder();
    }

    public PutTransactionBuilder updateTransaction(long id) {
        return new PutTransactionBuilder(id);
    }

    public class GetTransactionBuilder {
        public GetTransactionByIdBuilder byId(long id) {
            return new GetTransactionByIdBuilder(id);
        }

        public GetAllTransactionBuilder all() {
            return new GetAllTransactionBuilder();
        }
    }

    public class GetTransactionByIdBuilder {

        private long id;

        private GetTransactionByIdBuilder(long id) {
            this.id = id;
        }

        public MoneyTransactionViewDto get() {
            URI backendUri = UriComponentsBuilder.fromUriString(FinancesClient.BACKEND_URL)
                    .path("money_transactions/")
                    .path(String.valueOf(id))
                    .path(FinancesClient.FORMAT)
                    .queryParam("api_key", financesContext.getFinancesKey())
                    .build()
                    .toUri();

            MoneyTransactionViewWrapperDto transactionWrapper = restTemplate
                    .getForObject(backendUri, MoneyTransactionViewWrapperDto.class);
            return transactionWrapper.getMoneyTransaction();
        }
    }


    public class GetAllTransactionBuilder {

        private DateContext dateContext;

        public GetAllTransactionBuilder in(DateContext dateContext) {
            this.dateContext = dateContext;
            return this;
        }

        public List<MoneyTransactionViewDto> get() {
            URI backendUri = UriComponentsBuilder.fromUriString(FinancesClient.BACKEND_URL)
                    .path("money_transactions")
                    .path(FinancesClient.FORMAT)
                    .queryParam("api_key", financesContext.getFinancesKey())
                    .queryParam("start_on", dateContext.getFormattedStartDate("dd-MM-yyyy"))
                    .queryParam("end_on", dateContext.getFormattedEndDate("dd-MM-yyyy"))
                    .build()
                    .toUri();

            ResponseEntity<List<MoneyTransactionViewWrapperDto>> transactionWrappers = restTemplate
                    .exchange(
                            backendUri,
                            HttpMethod.GET,
                            null,
                            new ParameterizedTypeReference<List<MoneyTransactionViewWrapperDto>>() {
                            });

            return transactionWrappers.getBody()
                    .stream()
                    .map(MoneyTransactionViewWrapperDto::getMoneyTransaction)
                    .collect(Collectors.toList());
        }
    }


    public class PostTransactionDirectionBuilder {
        public PostTransactionBuilder withdrawal() {
            return new PostTransactionBuilder(MoneyTransactionCreateDto.Direction.WITHDRAWAL);
        }

        public PostTransactionBuilder deposit() {
            return new PostTransactionBuilder(MoneyTransactionCreateDto.Direction.DEPOSIT);
        }
    }

    public class PostTransactionBuilder {
        private MoneyTransactionCreateDto createDto;
        // TODO: Hardcoded wallet id
        private String clientAssignedId;

        private PostTransactionBuilder(MoneyTransactionCreateDto.Direction direction) {
            createDto = new MoneyTransactionCreateDto();
            createDto.setDirection(direction);
        }

        public PostTransactionBuilder name(String name) {
            createDto.setName(name);
            return this;
        }

        public PostTransactionBuilder amount(double amount) {
            createDto.setAmount(amount);
            return this;
        }

        public PostTransactionBuilder clientAssignedId(String id) {
            clientAssignedId = id;
            return this;
        }

        public PostTransactionBuilder date(LocalDate date) {
            createDto.setDate(date.toString());
            return this;
        }

        public PostTransactionBuilder tags(List<String> tags) {
            createDto.setTags(mapTagsToString(tags));
            return this;
        }

        public PostTransactionBuilder categoryId(CategoryId categoryId) {
            createDto.setCategoryId(categoryId.value());
            return this;
        }

        public PostTransactionBuilder walletId(Long walletId) {
            createDto.setWalletId(walletId);
            return this;
        }

        public MoneyTransactionViewDto save() {
            createDto.setClientAssignedId(clientAssignedId);

            logger.info("Saving to Kontomierz: {}", createDto);

            URI backendUri = UriComponentsBuilder.fromUriString(FinancesClient.BACKEND_URL)
                    .path("money_transactions")
                    .path(FinancesClient.FORMAT)
                    .queryParam("api_key", financesContext.getFinancesKey())
                    .build()
                    .toUri();


            MoneyTransactionWrapperDto<MoneyTransactionCreateDto> bodyWrapper = new MoneyTransactionWrapperDto<>();
            bodyWrapper.setMoneyTransaction(createDto);

            MoneyTransactionViewWrapperDto transactionWrapper = restTemplate
                    .postForObject(backendUri, bodyWrapper, MoneyTransactionViewWrapperDto.class);
            return transactionWrapper.getMoneyTransaction();
        }
    }

    //TODO refactor builders structure
    public class PutTransactionBuilder {
        private MoneyTransactionUpdateDto editDto = new MoneyTransactionUpdateDto();
        private long id;

        private PutTransactionBuilder(long id) {
            this.id = id;
        }

        public PutTransactionBuilder name(String name) {
            editDto.setName(name);
            return this;
        }

        public PutTransactionBuilder amount(double amount) {
            editDto.setAmount(amount);
            return this;
        }

        public PutTransactionBuilder date(LocalDate date) {
            editDto.setDate(date.toString());
            return this;
        }

        public PutTransactionBuilder tags(List<String> tags) {
            editDto.setTags(mapTagsToString(tags));
            return this;
        }

        public PutTransactionBuilder categoryId(CategoryId categoryId) {
            editDto.setCategoryId(categoryId.value());
            return this;
        }

        public PutTransactionBuilder walletId(Long walletId) {
            editDto.setWalletId(walletId);
            return this;
        }

        public MoneyTransactionViewDto save() {
            logger.info("Saving to Kontomierz: {}", editDto);

            URI backendUri = UriComponentsBuilder.fromUriString(FinancesClient.BACKEND_URL)
                    .pathSegment("money_transactions")
                    .path(String.valueOf(id))
                    .path(FinancesClient.FORMAT)
                    .queryParam("api_key", financesContext.getFinancesKey())
                    .build()
                    .toUri();

            MoneyTransactionWrapperDto<MoneyTransactionUpdateDto> bodyWrapper = new MoneyTransactionWrapperDto<>();
            bodyWrapper.setMoneyTransaction(editDto);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<MoneyTransactionWrapperDto> bodyEntity = new HttpEntity<>(bodyWrapper, headers);

            ResponseEntity<MoneyTransactionViewWrapperDto> responseEntity = restTemplate
                    .exchange(backendUri, HttpMethod.PUT, bodyEntity, MoneyTransactionViewWrapperDto.class);

            return responseEntity.getBody().getMoneyTransaction();
        }
    }

    private String mapTagsToString(List<String> tags) {
        return String.join(",", tags);
    }
}
