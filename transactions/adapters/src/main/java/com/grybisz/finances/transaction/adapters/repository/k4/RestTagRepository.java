package com.grybisz.finances.transaction.adapters.repository.k4;


import com.grybisz.finances.transaction.adapters.repository.k4.client.FinancesClient;
import com.grybisz.finances.transaction.adapters.repository.k4.client.dto.TagViewDto;
import com.grybisz.finances.transactions.usecase.port.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class RestTagRepository implements TagRepository {

    private FinancesClient financesClient;

    @Autowired
    public RestTagRepository(FinancesClient financesClient) {
        this.financesClient = financesClient;
    }

    @Override
    public List<String> getTags() {
        return mapToStrings(financesClient.getTags());
    }

    private List<String> mapToStrings(List<TagViewDto> tagsDtos) {
        return tagsDtos
                .stream()
                .map(TagViewDto::getName)
                .collect(toList());
    }
}
