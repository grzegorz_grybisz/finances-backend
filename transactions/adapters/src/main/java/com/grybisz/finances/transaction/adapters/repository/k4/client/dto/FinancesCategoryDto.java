package com.grybisz.finances.transaction.adapters.repository.k4.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancesCategoryDto {

    private static final String GROUP_ID = "category_group_id";
    private Long id;
    private String name;
    private Long groupId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty(GROUP_ID)
    public Long getGroupId() {
        return groupId;
    }

    @JsonProperty(GROUP_ID)
    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }
}
