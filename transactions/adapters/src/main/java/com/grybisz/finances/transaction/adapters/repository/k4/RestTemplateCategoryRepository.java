package com.grybisz.finances.transaction.adapters.repository.k4;

import com.grybisz.finances.transaction.adapters.repository.k4.client.FinancesClient;
import com.grybisz.finances.transaction.adapters.repository.k4.client.dto.FinancesCategoryDto;
import com.grybisz.finances.transaction.adapters.repository.k4.client.dto.FinancesCategoryGroupDto;
import com.grybisz.finances.transactions.domain.Category;
import com.grybisz.finances.transactions.domain.CategoryGroup;
import com.grybisz.finances.transactions.usecase.port.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class RestTemplateCategoryRepository implements CategoryRepository {

    private FinancesClient financesClient;

    private List<FinancesCategoryGroupDto> cache = new ArrayList<>();

    @Autowired
    public RestTemplateCategoryRepository(FinancesClient financesClient) {
        this.financesClient = financesClient;
    }

    @Override
    public Optional<Category> getCategory(Long id) {
        List<FinancesCategoryGroupDto> groups = getCategoryGroups();

        return groups.stream()
                .map(FinancesCategoryGroupDto::getCategories)
                .flatMap(Collection::stream)
                .filter(category -> category.getId().equals(id))
                .map(this::convertToEntity)
                .findFirst();
    }

    @Override
    public List<CategoryGroup> getGroupedCategories() {
        List<FinancesCategoryGroupDto> groups = getCategoryGroups();

        return groups
                .stream()
                .map(this::convertToEntity)
                .collect(Collectors.toList());
    }

    private List<FinancesCategoryGroupDto> getCategoryGroups() {
        if (cache.isEmpty()) {
            cache = financesClient.getCategories();
        }
        return cache;
    }

    private CategoryGroup convertToEntity(FinancesCategoryGroupDto dto) {
        String id = dto.getId().toString();
        String groupName = dto.getName();
        List<Category> categories = mapCategoriesToEntities(dto);

        return new CategoryGroup(id, groupName, categories);
    }

    private List<Category> mapCategoriesToEntities(FinancesCategoryGroupDto dto) {
        return dto.getCategories()
                .stream()
                .map(this::convertToEntity)
                .collect(Collectors.toList());
    }

    private Category convertToEntity(FinancesCategoryDto dto) {
        String id = dto.getId().toString();
        String name = dto.getName();
        return new Category(id, name);
    }
}
