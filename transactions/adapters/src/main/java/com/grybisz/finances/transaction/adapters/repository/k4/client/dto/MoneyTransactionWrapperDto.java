package com.grybisz.finances.transaction.adapters.repository.k4.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Grzesiek on 2017-01-15.
 */
public final class MoneyTransactionWrapperDto<T> {

    private T moneyTransaction;

    @JsonProperty("money_transaction")
    public T getMoneyTransaction() {
        return moneyTransaction;
    }

    @JsonProperty("money_transaction")
    public void setMoneyTransaction(T moneyTransaction) {
        this.moneyTransaction = moneyTransaction;
    }
}
