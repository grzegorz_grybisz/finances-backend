package com.grybisz.finances.transaction.adapters.repository.k4;

import com.grybisz.finances.transaction.adapters.repository.k4.client.FinancesClient;
import com.grybisz.finances.transaction.adapters.repository.k4.client.dto.FinancesUserAccountDto;
import com.grybisz.finances.transactions.domain.Wallet;
import com.grybisz.finances.transactions.usecase.port.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class RestWalletRepository implements WalletRepository {

    private FinancesClient financesClient;

    private List<FinancesUserAccountDto> cache = new ArrayList<>();

    @Autowired
    public RestWalletRepository(FinancesClient financesClient) {
        this.financesClient = financesClient;
    }

    @Override
    public Optional<Wallet> getWallet(Long id) {
        return getWallets()
                .stream()
                .filter(wallet -> wallet.getId().equals(id.toString()))
                .findFirst();
    }

    @Override
    public List<Wallet> getWallets() {
        List<FinancesUserAccountDto> wallets = queryWallets();

        return wallets.stream()
                .map(this::convertToEntity)
                .collect(Collectors.toList());
    }

    private List<FinancesUserAccountDto> queryWallets() {
        if (cache.isEmpty()) {
            cache = financesClient.getWallets();
        }
        return cache;
    }


    private Wallet convertToEntity(FinancesUserAccountDto dto) {
        Wallet wallet = new Wallet();

        wallet.setId(dto.getId().toString());
        wallet.setName(dto.getName());
        wallet.setAmount(dto.getBalance());

        return wallet;
    }
}
