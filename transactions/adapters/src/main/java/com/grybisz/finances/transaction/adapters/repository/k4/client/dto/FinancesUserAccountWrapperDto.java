package com.grybisz.finances.transaction.adapters.repository.k4.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancesUserAccountWrapperDto {

    private static final String ACCOUNT_PROPERTY = "user_account";

    private FinancesUserAccountDto userAccount;

    @JsonProperty(ACCOUNT_PROPERTY)
    public FinancesUserAccountDto getUserAccount() {
        return userAccount;
    }

    @JsonProperty("user_account")
    public void setUserAccount(FinancesUserAccountDto userAccount) {
        this.userAccount = userAccount;
    }
}

