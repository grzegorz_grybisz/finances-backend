package com.grybisz.finances.transaction.adapters.repository.k4.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancesUserAccountDto {

    private static final String NAME_PROPERTY = "display_name";

    private Long id;
    private String name;
    private Double balance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty(NAME_PROPERTY)
    public String getName() {
        return name;
    }

    @JsonProperty(NAME_PROPERTY)
    public void setName(String name) {
        this.name = name;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }
}
