package com.grybisz.finances.transaction.adapters.repository.k4.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Grzesiek on 2017-01-15.
 */
public final class MoneyTransactionViewWrapperDto {

    private MoneyTransactionViewDto moneyTransaction;

    @JsonProperty("money_transaction")
    public MoneyTransactionViewDto getMoneyTransaction() {
        return moneyTransaction;
    }

    @JsonProperty("money_transaction")
    public void setMoneyTransaction(MoneyTransactionViewDto moneyTransaction) {
        this.moneyTransaction = moneyTransaction;
    }
}
