package com.grybisz.finances.transaction.adapters.repository.k4.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.grybisz.finances.transactions.domain.EntityToJsonFormatter;

/**
 * Created by Grzesiek on 2016-12-25.
 */
public final class MoneyTransactionViewDto {

    private static final String WALLET_ID_PROPERTY = "user_account_id";
    private static final String ID_PROPERTY = "category_id";
    private static final String NAME_PROPERTY = "category_name";
    private static final String DATE_PROPERTY = "transaction_on";
    private static final String TAGS_PROPERTY = "tag_string";

    private long id;
    private String description;
    private double amount;
    private long categoryId;
    private String categoryName;
    private String tags;
    private String date;
    private long walletId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @JsonProperty(ID_PROPERTY)
    public long getCategoryId() {
        return categoryId;
    }

    @JsonProperty(ID_PROPERTY)
    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    @JsonProperty(NAME_PROPERTY)
    public String getCategoryName() {
        return categoryName;
    }

    @JsonProperty(NAME_PROPERTY)
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @JsonProperty(DATE_PROPERTY)
    public String getDate() {
        return date;
    }

    @JsonProperty(DATE_PROPERTY)
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty(TAGS_PROPERTY)
    public String getTags() {
        return tags;
    }

    @JsonProperty(TAGS_PROPERTY)
    public void setTags(String tags) {
        this.tags = tags;
    }

    @JsonProperty(WALLET_ID_PROPERTY)
    public long getWalletId() {
        return walletId;
    }

    @JsonProperty(WALLET_ID_PROPERTY)
    public void setWalletId(long walletId) {
        this.walletId = walletId;
    }

    @Override
    public String toString() {
        return EntityToJsonFormatter.toPrettyJsonString(this);
    }
}
