package com.grybisz.finances.transaction.adapters.repository.k4.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MoneyTransactionInputDto {

    private static final String WALLET_ID_PROPERTY = "user_account_id";
    private static final String AMOUNT_PROPERTY = "currency_amount";
    private static final String TAG_PROPERTY = "tag_string";
    private static final String DATE_PROPERTY = "transaction_on";
    private static final String CATEGORY_PROPERTY = "category_id";

    private String name;
    private double amount;
    private String tags;
    private String date;
    private long categoryId;
    private long walletId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty(AMOUNT_PROPERTY)
    public String getAmount() {
        return String.valueOf(amount);
    }

    @JsonProperty(AMOUNT_PROPERTY)
    public void setAmount(double amount) {
        this.amount = amount;
    }

    @JsonProperty(TAG_PROPERTY)
    public String getTags() {
        return tags;
    }

    @JsonProperty(TAG_PROPERTY)
    public void setTags(String tags) {
        this.tags = tags;
    }

    @JsonProperty(DATE_PROPERTY)
    public String getDate() {
        return date;
    }

    @JsonProperty(DATE_PROPERTY)
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty(CATEGORY_PROPERTY)
    public long getCategoryId() {
        return categoryId;
    }

    @JsonProperty(CATEGORY_PROPERTY)
    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    @JsonProperty(WALLET_ID_PROPERTY)
    public long getWalletId() {
        return walletId;
    }

    @JsonProperty(WALLET_ID_PROPERTY)
    public void setWalletId(long walletId) {
        this.walletId = walletId;
    }
}
