package com.grybisz.finances.transaction.adapters.repository.k4.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import com.grybisz.finances.transactions.domain.EntityToJsonFormatter;

/**
 * Created by Grzesiek on 2016-12-25.
 */
public final class MoneyTransactionCreateDto extends MoneyTransactionInputDto {

    private Direction direction;
    private String clientAssignedId;

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    @JsonProperty("client_assigned_id")
    public String getClientAssignedId() {
        return clientAssignedId;
    }

    @JsonProperty("client_assigned_id")
    public void setClientAssignedId(String clientAssignedId) {
        this.clientAssignedId = clientAssignedId;
    }

    public enum Direction {
        WITHDRAWAL,
        DEPOSIT;

        @JsonValue
        @Override
        public String toString() {
            return name().toLowerCase();
        }
    }

    @Override
    public String toString() {
        return EntityToJsonFormatter.toPrettyJsonString(this);
    }
}
