package com.grybisz.finances.transactions.domain;

import java.util.Objects;
import java.util.Optional;

public class TransactionId {

    private String id;

    private TransactionId() {
    }

    private TransactionId(String id) {
        this.id = id;
    }

    public static TransactionId newId() {
        return new TransactionId();
    }

    public static TransactionId from(String transactionId) {
        return new TransactionId(transactionId);
    }

    public Optional<String> value() {
        return Optional.ofNullable(id);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof TransactionId) {
            TransactionId otherId = (TransactionId) obj;
            return Objects.equals(id, otherId.id);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return EntityToJsonFormatter.toPrettyJsonString(this);
    }
}
