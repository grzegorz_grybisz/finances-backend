package com.grybisz.finances.transactions.domain;

import java.util.Objects;

public class CategoryId {

    private Long id;

    private CategoryId(Long id) {
        this.id = id;
    }

    public static CategoryId from(String id) {
        return new CategoryId(Long.valueOf(id));
    }

    public static CategoryId from(Long id) {
        return new CategoryId(id);
    }

    public Long value() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof CategoryId) {
            CategoryId that = (CategoryId) o;
            return Objects.equals(id, that.id);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "CategoryId: " + id;
    }
}
