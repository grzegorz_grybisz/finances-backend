package com.grybisz.finances.transactions.domain;

public enum TransactionType {
    EXPENSE, INCOME;
}
