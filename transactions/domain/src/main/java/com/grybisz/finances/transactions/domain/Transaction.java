package com.grybisz.finances.transactions.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Grzesiek on 2016-12-18.
 */
public class Transaction {

    private TransactionId id;
    private String name;
    private double amount;
    private TransactionType type;
    private LocalDate date;

    private CategoryId category;
    private Long wallet;
    private List<String> tags = new ArrayList<>();

    public Transaction() {
        id = TransactionId.newId();
    }

    public TransactionId getId() {
        return id;
    }

    public void setId(TransactionId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public void setCategory(CategoryId category) {
        this.category = category;
    }

    public CategoryId getCategory() {
        return category;
    }

    public Long getWallet() {
        return wallet;
    }

    public void setWallet(Long wallet) {
        this.wallet = wallet;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        return EntityToJsonFormatter.toPrettyJsonString(this);
    }
}
