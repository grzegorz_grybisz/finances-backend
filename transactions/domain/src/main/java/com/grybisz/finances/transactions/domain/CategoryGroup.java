package com.grybisz.finances.transactions.domain;

import java.util.List;

public class CategoryGroup {

    private String id;
    private String name;
    private List<Category> categories;

    public CategoryGroup(String id, String name, List<Category> categories) {
        this.id = id;
        this.name = name;
        this.categories = categories;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Category> getCategories() {
        return categories;
    }
}
