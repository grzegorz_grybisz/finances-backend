package com.grybisz.finances.transactions.domain;

import static com.grybisz.finances.transactions.domain.EntityToJsonFormatter.toPrettyJsonString;

public class Wallet {

    private String id;

    private String name;
    private Double amount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return toPrettyJsonString(this);
    }
}
