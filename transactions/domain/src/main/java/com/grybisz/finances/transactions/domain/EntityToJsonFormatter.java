package com.grybisz.finances.transactions.domain;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class EntityToJsonFormatter {

    public static String toPrettyJsonString(Object object) {
        try {
            return System.lineSeparator()
                    + object.getClass().getSimpleName()
                    + getPrettyMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
    }

    private static ObjectMapper getPrettyMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        return mapper;
    }
}
